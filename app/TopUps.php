<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopUps extends Model
{
    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function Orders()
    {
        return $this->hasMany('App\Orders');
    }

    public function order()
    {
        return $this->morphMany(Orders::class, 'orderable');
    }
}
