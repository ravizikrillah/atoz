<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\CancelOrder;

use App\Http\Requests;
use App\TopUps;
use App\Orders;
use Auth;
use Validator;
use DB;

class OrdersController extends Controller
{

	public function create_order(Request $request, $order_no)
    {

        if (is_null($order = Orders::with('orderable')->findOrFail($order_no))) {
            die('Order Number does not exists');
        }

        return view('success', compact('order'));
    }

    public function get_payment(Request $request, $order_no)
    {


        if (is_null($order = Orders::findOrFail($order_no))) {
            die('Order Number does not exists');
        }


        $request->merge($order->toArray());
        $request->flash();

        return view('payment', compact('order'));
    }

    public function post_payment(Request $request, $order_no)
    {

        if (is_null($order = Orders::findOrFail($order_no))) {
          die('Order Number does not exists');
        }

        $validate = Validator::make($request->all(), [
          
          'order_no' => 'required',

        ]);


        if ($validate->fails())
        {
            return redirect('payment/'.$order_no)->withErrors($validate)->withInput($request->all());
        }

        if($order->orderable_type == "App\TopUps"){
            //Create Success Rate
            $random = rand(1,100);

            //9 AM(9:00) samapai 5PM (17:00)
            if ((date('Hi') > 900) && (date('Hi') < 1700))  {
            	//90% Rate Success
       			if($random <= 90){ 
            		$order->status   = "Success";
       			}else{
            		$order->status   = "Failed";
       			}
    		}
    		else{
    			// 40% Rate Success
    			if($random <= 40){ 
            		$order->status   = "Success";
       			}else{
            		$order->status   = "Failed";
       			}
    		}

            $order->save();
        }  
        elseif($order->orderable_type == "App\Products"){

            $characters  ='1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
            $charactersLength = strlen($characters);
            $shipping_code = '';
            for($i=0;$i<8;$i++) {
                $shipping_code .= $characters[rand(0, $charactersLength - 1)];
            }

            $order->status   = $shipping_code;
            $order->save();

        }

        return redirect('order');
    }

    public function order_history(Request $request)
    {

        $search = $request->search;


        if($search == null){
            $order_history = Orders::history()->paginate(20);   
        }
        elseif($search != null){
            $order_history = Orders::history()->where('order_no' ,'like','%'.$search.'%')->paginate(20); 
        }                     

        return view('order_history', compact('order_history','search'));
        
    }
    
}
