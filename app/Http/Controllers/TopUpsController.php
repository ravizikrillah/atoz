<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\CancelOrder;   

use App\Http\Requests;
use App\TopUps;
use App\Orders;
use Auth;
use Validator;
use DB;
use Carbon;
use mysql_query;

class TopupsController extends Controller
{

    public function index()
    {
        return view('prepaid');
    }

    public function prepaid(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'mobile_phone'    => 'required',
            'value'           => 'required',

        ]);

        if ($validate->fails())
        {
          return redirect('/prepaid')->withErrors($validate)->withInput($request->all());

        }

        DB::beginTransaction();
        try{

            $topups = new TopUps;

            $data = DB::table('top_ups')
                    ->where('id', DB::raw ("(select max(`id`) from top_ups)"))
                    ->pluck('id');

            $dataIsset = isset($data[0]);
            if ($dataIsset ==  TRUE){
                $topups->id=$data[0]+'1';
            }else{
                $topups->id=1;
            }


            $topups->mobile_phone  = $request->mobile_phone;
            $topups->value         = $request->value;
            $topups->save();

            $order = new Orders;

            for ($randomNumber = mt_rand(1, 9), $i = 1; $i < 10; $i++) {
                    $randomNumber .= mt_rand(0, 9);
            }

            $order->order_no        = $randomNumber;
            $order->orderable_id    = $topups->id;
            $order->orderable_type  = "App\TopUps";
            $order->status          = "Pay now";
            $order->users_id        = Auth::user()->id;
            $order->save();

            CancelOrder::dispatch()->delay(Carbon::now()->addMinutes(5));

            DB::commit();

            return redirect('/create_order/'.$randomNumber);

        }catch(\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
            return redirect()->back();
        }

    }

}
