<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\CancelOrder;

use App\Http\Requests;
use App\Products;
use App\Orders;
use Auth;
use Validator;
use DB;
use Carbon;


class ProductsController extends Controller
{
    
    public function index()
    {
        return view('product');
    }

        public function post_product(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'product'    		=> 'required',
            'shipping_address'  => 'required',
            'price'           	=> 'required',

        ]);

        if ($validate->fails())
        {
          return redirect('/product')->withErrors($validate)->withInput($request->all());

        }

        DB::beginTransaction();
        try{

            $products = new Products;

            $data = DB::table('products')
                    ->where('id', DB::raw ("(select max(`id`) from products)"))
                    ->pluck('id');

            $dataIsset = isset($data[0]);
            if ($dataIsset ==  TRUE){
                $products->id=$data[0]+'1';
            }else{
                $products->id=1;
            }

            $products->product  			= $request->product;
            $products->shipping_address     = $request->shipping_address;
            $products->price     			= $request->price;
            $products->save();

            $order = new Orders;

            for ($randomNumber = mt_rand(1, 9), $i = 1; $i < 10; $i++) {
                    $randomNumber .= mt_rand(0, 9);
            }

            $order->order_no        = $randomNumber;
            $order->orderable_id    = $products->id;
            $order->orderable_type  = "App\Products";
            $order->status          = "Pay now";
            $order->users_id        = Auth::user()->id;
            $order->save();

            CancelOrder::dispatch()->delay(Carbon::now()->addMinutes(5));

            DB::commit();

            return redirect('/create_order/'.$randomNumber);

        }catch(\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
            return redirect()->back();
        }

    }

}
