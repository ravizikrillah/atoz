<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;


class Orders extends Model
{
    //Softdelete
    use SoftDeletes;
    protected $dates =['deleted_at'];
    protected $primaryKey = 'order_no';
    protected $casts = ['order_no' => 'string'];

    // //Relation
    public function TopUps()
    {
        return $this->belongsTo('App\TopUps', 'orderable_id');
    }

    //Relation 
    public function Products()
    {
        return $this->belongsTo('App\Products', 'orderable_id');
    }

    //Morph Relation
    public function orderable()
    {
        return $this->morphTo();
    }

    //Scope
    public function scopeHistory($query)
    {
        return $query->where('users_id', Auth::user()->id )->orderBy('created_at','DESC');
    }

    //Accessor
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d H:i:s');
    }
}
