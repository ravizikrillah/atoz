<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;
    protected $dates =['deleted_at'];

    public function Orders()
    {
        return $this->hasMany('App\Orders');
    }

    public function order()
    {
        return $this->morphMany(Orders::class, 'orderable');
    }

    //Mutator
    public function setProductAttribute($value) 
	{
   		$this->attributes['Product'] = strtoupper($value);
	}
    
    //Mutator
    public function setShippingAddressAttribute($value) 
    {
        $this->attributes['Shipping_Address'] = strtoupper($value);
    }

}
