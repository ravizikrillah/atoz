@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Prepaid Balance</div>

                <div class="panel-body">
                    <form method="post" role="form" action="{{ URL::to('prepaid')}}">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <input name="mobile_phone" type="text" class="form-control" placeholder="Mobile Number " minlength="7" maxlength="12"  pattern="(\()?(081)(\d{0,9})?\)?" required="required" value="{{ old('mobile_phone') }}" step="1" title="Format number start with 081 and has 7 - 12 digits">
                            </div>
                            <div class="form-group">
                                <select name="value"  required="required" class="form-control select2">
                                <option value="" selected="selected" disabled="disabled">Select Value</option>
                                <option value="10000" >10.000</option>
                                <option value="50000" >50.000</option>
                                <option value="100000" >100.000</option>
                                </select>
                            </div>
                            <div class="p-a text-right">
                                <button type="submit" class="btn btn-primary"><b>Submit</b></button>
                            </div>
                   </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
