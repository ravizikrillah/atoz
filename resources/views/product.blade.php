@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Prepaid Balance</div>

                <div class="panel-body">
                    <form method="post" role="form" action="{{ URL::to('product')}}">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <textarea  name="product" class="form-control" placeholder="Product" value="{{ old('product') }}" rows="3" minlength="10" maxlength="150" required="required"></textarea>
                            </div>
                            <div class="form-group">
                                <textarea  name="shipping_address" class="form-control" placeholder="Shipping Address" value="{{ old('shipping_address') }}" rows="5" minlength="10" maxlength="150" required="required"></textarea>
                            </div>
                            <div class="form-group">
                                <input  name="price" type="number" class="form-control" placeholder="Price" value="{{ old('price') }}" rows="3" required="required">
                            </div>
                            <div class="p-a text-right">
                                <button type="submit" class="btn btn-primary"><b>Submit</b></button>
                            </div>
                   </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
