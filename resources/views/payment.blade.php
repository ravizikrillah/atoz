@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading"><b>Pay Your Order</b></div>

                    <form method="post" role="form" action="{{ URL::to('payment/'.$order->order_no)}}">
                        <div class="panel-body">
                            {{ csrf_field() }}
                                <div class="form-group">
                                    <input name="order_no" type="text" class="form-control" value="{{ $order->order_no }}" >
                                </div>
                        </div>
                        <div class="panel-footer">
                             <button type="submit" class="btn btn-primary btn-lg"><b>Submit</b></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
