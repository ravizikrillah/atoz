@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading"><b>Success !</b></div>
                
                    <div class="panel-body">
                    	<div class="form-group">
                            <div class="col-md-6">
                    		  <label>Order No</label>
                            </div>
           					<b>{{ $order->order_no }}</b>
                    	</div>
                    	<div class="form-group">
                            <div class="col-md-6">
                    		  <label>Total</label>
                            </div>

                            @if($order->orderable_type == "App\TopUps")
           					    <b>Rp {{ number_format((($order->TopUps->value) + ($order->TopUps->value*5/100))) }}</b>
                            @elseif($order->orderable_type == "App\Products")
                                <b>Rp {{ number_format($order->Products->price + 10000) }}</b>
                            @endif

                    	</div>
                    	<div class="form-group">
                            @if($order->orderable_type == "App\TopUps")
                    		    Your mobile phone number <b>{{$order->TopUps->mobile_phone }}</b> will recive <b>Rp {{ number_format($order->TopUps->value) }}</b>
                            @elseif($order->orderable_type == "App\Products")
                                <b>{{$order->Products->product }}</b> that cost <b>{{$order->Products->price}}</b> will be shipped to: <br>
                                {{$order->Products->shipping_address }} <br>
                                only after you pay
                            @endif

                    	</div>
                        <div class="form-group">
                            <span>
                                {{$order->created_at }}
                            </span>
                        </div>

                    </div>

                    <form method="post" role="form" action="{{ URL::to('payment/'.$order->order_no)}}">
                        {{ csrf_field() }}
                        <div class="panel-footer">
                            <button class="btn btn-primary btn-lg" type="submit"><b>  Pay now  </b></button>
                        </div>
                    </form>              

                </div>
            </div>
        </div>
    </div>
</div>
@stop