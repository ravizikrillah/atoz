@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-warning ">
                <div class="panel-heading"><b>Order History</b></div>

                    <div class="panel-body"> 
                        <div class="row">
                            <div class="col-lg-12 ">
                                <form method="post">
                                    {{ csrf_field() }}
                                    <input type="search" name="search" value="{{$search}}" class="form-control" placeholder="Search by Order no." title="Search">
                                </form>

                            </div> 
                        </div>  
                        <br>
                        <div class="row">
                            <div class="col-lg-12 ">
                                <table class="table table-bordered " cellspacing="0" cellpadding="0">
                                    <tbody>
                                        @foreach($order_history as $orders)
                                        <tr>
                                            <td>
                                            @if($orders->orderable_type == "App\TopUps")
                                                {{$orders->order_no}} &emsp;&emsp;&emsp;&emsp;Rp. {{ number_format((($orders->TopUps->value) + ($orders->TopUps->value*5/100))) }} <br>
                                                <b>{{number_format($orders->TopUps->value , 0, ',', '.')}} for {{$orders->TopUps->mobile_phone}}</b>
                                            @elseif($orders->orderable_type == "App\Products")
                                                {{$orders->order_no}} &emsp;&emsp;&emsp;&emsp;Rp. {{ number_format((($orders->Products->price) + 10000)) }} <br>
                                                <b>{{$orders->Products->product}} that cost {{ number_format((($orders->Products->price) + 10000)) }}
                                            @endif

                                            </td>

                                            <td style="vertical-align : middle;text-align:center;">
                                                @if($orders->status == "Pay now")
                                                    <a href="{{ URL::to('payment/'.$orders->order_no)}}" type="button" class="btn btn-primary"><b>{{$orders->status}}</b></a>
                                                @elseif ($orders->status == "Success")
                                                    <b style="color: green" title="Balance is successfully paid">{{$orders->status}}</b>
                                                @elseif ($orders->status == "Failed")
                                                    <b style="color: orange" title="Balance transaction failed">{{$orders->status}}</b>
                                                @elseif ($orders->status == "Canceled")
                                                    <b style="color: red">{{$orders->status}}</b>
                                                @else
                                                    <b>shipping code <br>{{$orders->status}}</b>
                                                @endif
                                            </td>
                                        </tr>
                                       
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        </div>  

                    </div>

                    <div class="panel-footer">
                        <div align="center">{{ $order_history->links('pagination.default') }}</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection


