<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Atoz') }}</title>

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style type="text/css">
    .nav-space {
        padding: 10px 10px;font-size: 20px; line-height: 2
    }
</style>

<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                @guest
                    <div class="nav-space">
                        ATOZ
                    </div>
                @else
                <?php
                    if(Auth::user()->name != null){
                        $FullName = explode(' ', Auth::user()->name);
                        $Name = $FullName[0];
                    }else{
                        $Name = Auth::user()->email;
                    }

                    $unpaid = DB::table('orders')
                                ->where('users_id', Auth::user()->id)
                                ->where('status', "Pay now")
                                ->count();
                ?>
                    <div style="padding: 10px 10px; line-height: 1.4">
                        <b style="color: black; font-size: 15px;">Hello, {{$Name}}</b><br>
                        <a href="{{ URL::to('order')}}">
                            <b style="color: red; font-size: 12px">{{$unpaid}}</b>
                            <b style="color: grey; font-size: 12px">Unpaid Order</b>
                        </a>
                    </div>

                </div>
                <div style="margin-left: 200px">
                    <div class="navbar-header">
                        <div class="nav-space">
                            <a href="{{ url('prepaid') }}"><b> Prepaid Balance</b> </a>
                        </div>
                    </div>
                    <div class="navbar-header">
                        <div class="nav-space">
                            |
                        </div>
                    </div>
                    <div class="navbar-header">
                        <div class="nav-space">
                            <a href="{{ url('product') }}"><b> Product Page </b></a>
                        </div>     
                    </div>
                @endguest
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
