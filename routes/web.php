<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['web','auth','User']], function ()
{
	Route::group(['prefix' => '/'], function () {
		Route::get('/prepaid', 'TopupsController@index');
		Route::post('/prepaid', 'TopupsController@prepaid');

		Route::get('/product', 'ProductsController@index');
		Route::post('/product', 'ProductsController@post_product');

		Route::get('/create_order/{order_no}', 'OrdersController@create_order');

		Route::get('/payment/{order_no}', 'OrdersController@get_payment');
		Route::post('/payment/{order_no}', 'OrdersController@post_payment');

		Route::get('/order', 'OrdersController@order_history');
		Route::post('/order', 'OrdersController@order_history');
	});

});
	